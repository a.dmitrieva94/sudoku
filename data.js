const arr = [
    [1,2,3,4,5,6,7,8,9],
    [4,5,6,7,8,9,1,2,3],
    [7,8,9,1,2,3,4,5,6],
    [2,3,4,5,6,7,8,9,1],
    [5,6,7,8,9,1,2,3,4],
    [8,9,1,2,3,4,5,6,7],
    [3,4,5,6,7,8,9,1,2],
    [6,7,8,9,1,2,3,4,5],
    [9,1,2,3,4,5,6,7,8]
]

const row = (start) => {
    const arrDouther = []
    var current = start
    for (let i = 0; i < 9; i++) {
        arrDouther[i] = current;
        if (current === 9){
            current = 0
        }
        current += 1;
    }
    return arrDouther
}

const canSum = (current, num, max) => {
    return current+num <= max ? current + num : current - max + num
}

const shiftGenerate = () => {
    var start = Math.floor(Math.random() * (9 - 1)) + 1;
    var current = start
    const result = []
    for (let x = 0; x < 9; x++) {
        result[x] = row(current);
        if (x === 2) {
            current = canSum(start, 1, 9)
        } else if (x === 5) {
            current = canSum(start, 2, 9)
        } else {
            current= canSum(current, 3, 9)
        }
    }
    return result
}
var arr2 = shiftGenerate()


const data = arr2.map(function(row) {
    return row.map(function(elem, i){
        const rand = Math.random() > 0.5
        return {
            value: elem,
            current:  rand ? elem : null,
            prefilled: rand
        }
    })
})

export default data
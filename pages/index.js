import React from 'react'
import ReactDOM from 'react-dom'

class App extends React.Component{
    state = {
        currentActive: null,
        data: null,
    }
    clickByCell = elem => {
        this.setState({
            currentActive: elem
        })
    }
    clickByButton = num =>{
        if(this.state.currentActive.prefilled){
            return
        }
        this.state.currentActive.current = num;
        this.forceUpdate()
    }
    componentDidMount() {
        import('../data').then(data => {
            this.setState({
                data: data.default
            })
        })
    }
    render() {
        const {
            currentActive,
            data,
        } = this.state

        return (
            <>
            <div className='wrapper'>
                <div className='wrap'>
                    {data && data.map((row, i) => {
                        return <div className='row' key={i}>
                            {row.map((elem, elemI) => {
                                let className = 'cell'
                                if (i % 3 === 0) {
                                    className+=' top'
                                }
                                if (i === 8) {
                                    className+=' bottom'
                                }
                                if (elemI % 3 === 0){
                                    className+=' left'
                                }
                                if (elemI === 8){
                                    className+=' right'
                                }
                                if (elem === currentActive) {
                                    className+=' active'
                                }
                                if(
                                    currentActive !== null 
                                    && elem.current !== null 
                                    && currentActive.current === elem.current 
                                    && elem !== currentActive
                                ){
                                    className+=' passive'
                                }
                                if(elem.current !== null && elem.current !== elem.value){
                                    className+=' error'
                                }

                                return <div className={className} key={elem.value} onClick={() => {this.clickByCell(elem)}}>
                                    {elem.current}
                                </div>
                            })
                            }
                        </div>
                    })}
                    
                </div>
                <div className='buttons'>
                    {[1,2,3,4,5,6,7,8,9].map((num) =>
                        <button key={num} onClick={() => this.clickByButton(num)}>{num}</button>
                    )}
                </div>
            </div>
            <style jsx>{`
                        .wrapper{
                            height: 100vh;
                            display: flex;
                            justify-content: center;
                            align-items: center;
                            flex-direction: column;
                        }
                        .row {
                            display: flex;
                        }
                        .cell{
                            width: 50px;
                            height: 50px;
                            box-shadow: inset 0 0 1px black;
                            text-align: center;
                            font-size: 40px;
                        }
                        *{
                            box-sizing: border-box;
                        }
                        .top{
                            border-top: 1px solid black;
                        }
                        .bottom{
                            border-bottom: 1px solid black;
                        }
                        .left{
                            border-left: 1px solid black;
                        }
                        .right{
                            border-right: 1px solid black;
                        }
                        .active{
                            background: aqua;
                            border: 1px solid blue;
                        }
                        .error{
                            background: red;
                            color: #fff;
                        }
                        .passive{
                            background: #bff4f5;
                        }
                    `}</style>
            </>
        )
    }
}


ReactDOM.render(<App/>, document.getElementById('root'))
export default App